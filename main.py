import shlex
with open(input("Входной файл: ")) as f:
	prog = f.read().split('\n')
out = []
for i in range(len(prog)):
	line = shlex.split(prog[i])
	if line[0] == "AGENT":
		out.append("namespace "+line[1]+" {")
	elif line[0] == "END":
		try:
			if line[1] == "RECORD":
				out.append("};")
			else:
				out.append("}")
		except IndexError:
			out.append("}")
	elif line[0] == "USE":
		out.append("#include "+'"'+line[1]+'"')
	elif line[0] == "IF":
		out.append("if("+line[1]+"){")
	elif line[0] == "ELSIF":
		out.append("else if("+line[1]+"){")
	elif line[0] == "ELSE":
		out.append("else{")
	elif line[0] == "FOR":
		out.append("for(int "+line[1]+"="+line[2]+";"+line[1]+"<"+line[3]+";"+line[1]+"+="+line[4]+"){")
	elif line[0] == "WHILE":
		out.append("while("+line[1]+"){")
	elif line[0] == "CALL":
		out.append(line[1]+"::"+line[2]+"("+", ".join(line[3:])+");")
	elif line[0] == "LET":
		out.append(line[1]+" = "+line[2]+"::"+line[3]+"("+", ".join(line[4:])+");")
	elif line[0] == "SUB":
		out.append("void "+line[1]+"(){")
	elif line[0] == "RECORD":
		out.append("struct "+ line[1]+" {")
	elif line[0] in ["INT", "STR", "DOUBLE", "BOOL", "CHAR", "COMPLEX"]:
		out.append(line[0]+" "+line[1]+";")
with open(input("Выходной файл: "), "w") as f:
	f.write("\n".join(out))